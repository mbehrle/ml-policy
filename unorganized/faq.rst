What is the background?
-----------------------

Deep learning started to affect our daily life. For example, unlocking mobile
phones via facial verification has become a standard feature. More than that,
people are still striving forward to try to find more interesting applications
of deep learning and to convert them into comercial products. Someday in the
future, deep learning application might also become popular in the free and
open source software ecosystem. At that time, linux distribution developers may
ask, whether some piece of intelligent software is really free in terms of
software freedom? This policy tries to explore this problem and shed some light
on the safety area where software freedom won't be threatened.

Previously this problem had been [discussed](https://lwn.net/Articles/760142/)
in Debian's development mailing list, but that thread drew no conclusion.  

What is machine learning / deep learning and deep learning applications?
------------------------------------------------------------------------

Generally there are several types of components in a deep learning software stack: (1) building block
library; (2) framework; (3) data or environment; (4) pre-trained model.
Building blocks (e.g. BLAS, LAPACK) and frameworks (e.g. caffe,
pytorch, tensorflow) are typical scientific software that don't need extra
consideration. In this document, we only focus on (3) and (4).

Machine learning / Deep learning algorithms enables the machine to learn from
data. These algorithms are able to deal with problems that are impossible to be
resolved manually, e.g. "What digit (0,1,...,9) is displayed on the picture?"
Taking k-NN (k Nearest Neighbor, the simplest machine learning algorithm) as an
example, the basic idea to solve the digit recognition problem is to memorize
how digits 0 to 9 look like, and compare any input with the memory and pick the
most similar one as the answer. The dataset from which the model memorizes the
looks of 0 to 9 are called "training dataset". Modern algorithms and models
are much more different or complex than the k-NN example, but they share some
similar points: learning from data, storing the learned "knowledge" (usually a
pile of high dimensional numerical arrays organized in a specific way), and
making predictions with its "knowledge". Things are a bit different for
reinforcement learning: the machine learns from an environment, e.g. a board
game simulator, instead of a dataset.

Here are some educational resources for your reference:
[Linear Algebra](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/)
[Matrix Methods in Machine Learning](https://ocw.mit.edu/courses/mathematics/18-065-matrix-methods-in-data-analysis-signal-processing-and-machine-learning-spring-2018/)
[Machine Learning](http://cs229.stanford.edu/)
[Computer Vision with Deep Learning](http://cs231n.stanford.edu/)
[Natural Language Processing with Deep Learning](http://web.stanford.edu/class/cs224n/)
[Theories in Deep Learining](https://stats385.github.io/)
[MIT OCW](https://ocw.mit.edu/index.htm)

Why is training data important?
-------------------------------

Without source code no program could be compiled. Without data or environment,
no model can be trained. Non-free source code compiles a non-free software.
Non-free data trains a non-free model.

Why is training program/script important?
-----------------------------------------

Without the training script, one cannot fully understand where the pre-trained
model came from. Absence of training program breaks one's freedom to study,
reproduce, or modify the model.

Reproducibility
---------------

When talking about reproducibility, there are several possibilities.  We use
"Numerically Reproducible" as the default definition of reproducibility.

**Numerically Reproducible** means a model's training curves (x-axis:
iteration, y-axis: numbers such as training accuracy, validation accuracy,
testing accuracy, loss value, etc) can be accurately reproduced under a certain
condition. A numerically reproducible model is not required to reproduce the
hashsum (e.g. MD5, SHA256).

Making machine learning models Numerically Reproducible is a *good and
important practice* to engineers and researchers for debugging. Being unable to
"Numerically" reproduce a model is expected to be a quality bug to upstream.
Generally Numerical Reproducibility can be achieved when the following
conditions hold:

1. The same software stack, same hardware configuration, same dataset, **same random seed**.
2. The linear algebra library is numerically stable and deterministic. (forward and backward computation)
3. Deterministic and reproducible random number generator given a fixed seed. (parameter initialization)
4. Deterministic and reproducible data batch sampler. (gradient)
5. No unexpected memory flip (overheat, cosmic radio, etc) during training.

Specifically, if a model is locally numerically reproducible, and the final
performance (e.g. classification accuracy on validation set) is very close to
the upstream reference model (or reference performance), the model is also
considered Numerically reproducible.

**Bit-by-Bit Reproducible**, a stronger form of reproducibility, refers
Numerical Reproducibility plus Hashsum Reproducibility. This may not be
easy to achieve, as hashsum identity may be easily broken by float number
precision issues, hidden timestamps, unstable object (especially Hash Table,
or say Dictionaries) serialization, or some other reasons.

**Academically Reproducible** is a much weaker form of reproducibility compared
to the "Numerical" form. "Academically reproducible" only requires the paper
authors to provide necessary information about their implementation. It doesn't
even require training program to be available.

*How to make sure a model is Numerically Reproducible?* Train a model twice or
more without changing any code or parameter, then compare the curves and digits
to see whether they match.

*Why is random seed mentioned?* Training usually begins from a randomly
picked point in the parameter space, and different initialization points
usually leads to slightly different training results. A fixed random seed makes
pseudo random number generator output reproducible, as well as the prameter
initialization, mini-batch sampling, etc.

Deep Learning Policy
--------------------

3. Packaging an intelligent software associated with any critical task must be
   discussed on -devel as deep neural networks introduced a new kind of
   vulnerability, that a network's response can be disrupted or even controlled by
   some carefully designed perturbations added to the network input (See
   reference: "Adversarial Examples"). Our security team cannot deal with
   such kind of vulnerability.
   The "critical task" mentioned here includes but is not limited to:
   (1) authentication (e.g. login via face verification or identification);
   (2) program execution (e.g. intelligent voice assistants: "Hey, Siri! sudo rm -rf /");
   (3) physical object manipulation (e.g. mechanical arms in non-educational occasion, cars i.e. auto pilot), etc.

**Additional notes to 1. and 2.** Models are hard to directly analyze. They aren't
source code. The only way you can tell what they do is to inspect the training
data and the training/inferencing program. Inferencing program could help one
understand what the model does, but it's far not enough to fully understand
the model. To be able to fully audit a deep learning program, the availability
of data and training program is necessary.

1. **Adversarial Examples** Deep neural networks are vulnerable to perturbations
that are not sensible to human. Don't try to ask AI model to deal with any
critical task unless one understands the security risk.
[The first paper that introduced adversarial examples](https://arxiv.org/abs/1312.6199) |
[Typical attacking method: FGSM (neural networks are too linear)](https://arxiv.org/abs/1412.6572) |
[Typical attacking method: PGD/I-BFGS]( https://arxiv.org/abs/1607.02533 ) |
[Targeted Attack on DeepSpeech (ASR)](https://arxiv.org/pdf/1801.01944.pdf)


Side Note: Unresolved Problems
------------------------------

**Dataset Size** It's always good if we can do these things purely with our
archive.  However sometimes it's just not easy to enforce: datasets used by DL
are generally large, (several hundred MB ~ several TB or even larger). For
example the wikipedia dump (see reference).

**Training Time** It's not easy to reproduce a big model. The most widely used
convolutional neural network (CNN), ResNet-152, typically requires 4 * Nvidia
GTX 1080Ti cards or better to train. Non-free driver usage is inevitable for
training on Nvidia GPU (CUDA). The state-of-the-art natural language
representation model, BERT (see reference), takes 2 weeks to train on TPU at a
cost about $500. Apart from that, modern deep learning frameworks supports
different computing devices, and users can usually switch computing device
smoothly without code change. That means most training programs can run on pure
CPU and is not bound to non-free software usage. However, CPU training can be
hundreds times slower than GPU or TPU training, even for some high-end Xeon
CPUs. For example, the training problem may surprisingly run on a cheap i3 CPU,
but the estimated training time will be way more surprising (maybe 1 year?).
(TODO: What about FPGA or alike?)

**GPL Specific problems**

There is no common infrastructure for tracking bugs and vulnerabilities in
machine learning models.

The questions may involve our contrib or non-free section.

1. Must the dataset for training a Free Model present in our archive?
   Wikipedia dump is a frequently used free dataset in the computational
   linguistics field. Is uploading wikipedia dump to our Archive sane?  See
   references: "wikimedia dumps".

2. Should we re-train the Free Models on buildd? This is crazy. Let's not do
   that right now. *"Either way I think a cross-community effort for retraining
   and reproducibility of models would be better than Debian having to do any
   retraining." -- pabs*

3. Can a ToxicCandy or Non-free model enter the non-free section?
   Is it acceptable?

4. Is a "Free Model" really free, if it takes 1 year to train on a purely
   free software stack, while just 1 hour on a non-free software stack?

2. **Wikimedia Dumps** *The current size of wikimedia dumps is 18T, but that
   includes several versions of data (five dated versions are shipped for most
   dumps), etc. As a sample, I think this is the english pages main text
   (not history or metadata), which is 15G compressed:
   https://ftp.acc.umu.se/mirror/wikimedia.org/dumps/enwiki/20190501/enwiki-20190501-pages-articles.xml.bz2*
   -- Mattias Wadenstein

3. **BERT** https://github.com/google-research/bert


5. [Pytorch-Reproducibility](https://pytorch.org/docs/stable/notes/randomness.html)

TODO
----

* How to interpret "source of an ML model"?
* GPL and ML model?
* The policy for ML model product? e.g. artistic creations, fonts etc.
