main:
	pandoc ML-Policy.rst -t html -o ML-Policy.html
	pandoc ML-Policy.rst -t latex -V fontfamily=times -V geometry=margin=1in --toc -N -o ML-Policy.pdf
clean:
	-rm ML-Policy.pdf ML-Policy.html
